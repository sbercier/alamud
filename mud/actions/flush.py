# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================


from .action import Action2, Action3
from mud.events import FlushEvent, FlushWithEvent

class FlushAction(Action2):
    EVENT = FlushEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "flush"

class FlushWithAction(Action3):
    EVENT = FlushWithEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"
    ACTION = "flush-with"
