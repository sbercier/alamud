# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================


from .action import Action2, Action3
from mud.events import BlendWithEvent


class BlendWithAction(Action3):
    EVENT = BlendWithEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"
    ACTION = "blend-with"
