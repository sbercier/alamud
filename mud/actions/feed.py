# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================


from .action import Action1, Action2, Action3
from mud.events import FeedEvent, FeedWithEvent


class FeedAction(Action2):
    EVENT = FeedEvent
    ACTION = "feed"
    RESOLVE_OBJECT = "resolve_for_use"

class FeedWithAction(Action3):
    EVENT = FeedWithEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"
    ACTION = "feed-with"
